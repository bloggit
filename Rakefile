require 'rubygems'
$: << 'lib'
require 'bloggit'
require 'util/fixed_hoe'
require 'bloggit/tasks/specs'
require 'bloggit/tasks/scm'

FixedHoe.new('bloggit', Bloggit::VERSION) do |p|
  p.rubyforge_name = 'bloggit'
  p.author = "M@ McCray"
  p.email = 'darthapo@gmail.com'
  p.summary = 'A static site generator for a blog with static pages'
  p.description = p.paragraphs_of('ReadMe', 2..5).join("\n\n")
  p.url = p.paragraphs_of('ReadMe', 0).first.split(/\n/)[2..-1].to_s.strip
  p.changes = p.paragraphs_of('History', 0..1).join("\n\n")
  p.rdoc_pattern = /^(lib|bin)(.*)\.rb$/
  
  p.extra_deps = [['activesupport'], ['RedCloth'], ['cmdparse'], ['BlueCloth'], ['builder'], ['markaby']]
end

Behaviors::ReportTask.new :specs do |t|
  t.pattern = 'test/**/*_test.rb'
  t.html_dir = 'docs'
  t.src_dir = 'docs/src'
end

task :default=>:test

Rake::TestTask.new do |t|
  t.libs << "lib"
  t.test_files = FileList['test/*/*test.rb']
  t.verbose = false
end

desc "Install gem"
task :install=>[:gem] do
  sh "sudo gem install pkg/bloggit-#{Bloggit::VERSION}.gem"
end


desc "Generates the documentation"
task :doc=>[:specs_src] do
  require 'bluecloth'
  require 'erb'
  require 'active_support'
  #RedCloth::DEFAULT_RULES.replace [:markdown, :textile]
  
  src_files = FileList['docs/src/*.markdown']
  src_filenames = src_files.map {|sf| File.basename sf, '.markdown' }
  src_titles = src_filenames.map {|sfn| sfn.gsub('_', ' ').titleize }
  layout_tmpl = ERB.new(File.readlines('docs/src/_layout.rhtml', 'r').join(''))

  puts "#{src_files.length} docs found.\n"  

  src_files.each do |src|
    filename = File.basename src, '.markdown'
    title = filename.gsub('_', ' ').titleize
    content = BlueCloth.new(File.readlines(src, 'r').join('')).to_html
    page_html = layout_tmpl.result(binding)
    File.open("docs/#{filename}.html", 'w' ) do |f|
      f.write( page_html )
    end
  end
  
  puts "Done."
end
