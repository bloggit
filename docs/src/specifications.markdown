
## Bloggit should: 


* Do something?


## Page should: 


* raise an error if trying to load a `Page` from a non-existant file
* parse `Page` from file
* parse status and slug


## Post should: 


* raise an error if trying to load a `Post` from a non-existant file
* parse `Post` from file
* parse publish date and slug


## Site should: 


* not allow `Site.new`
* not create a `Site` object unless path is a directory
* create a `Site` object from a file path


## Template should: 


* render templates from text
* generate URLs correctly
* render templates from file


## Text Formatter should: 


* render text using Textile
* render text using Markdown
* raise an error when trying to use an undefined format
* allow registration of custom text formatter


