require 'rubygems'
require 'yaml'
require 'redcloth'
require 'bluecloth'
require 'bloggit/etc/hash'
require 'bloggit/etc/string'
require 'active_support'
require 'builder'

module Bloggit
  RELEASE_INFO = [1, 1, 0]
  VERSION = RELEASE_INFO.join('.')
end

class Time
  def to_y
    self.to_yaml.slice(4..-1).strip
  end
end

require 'bloggit/hooks'
require 'bloggit/media'
require 'bloggit/page'
require 'bloggit/post'
require 'bloggit/site'
require 'bloggit/tag'
require 'bloggit/template'
require 'bloggit/text_formatter'
require 'bloggit/checksum'
require 'bloggit/generator'
require 'bloggit/publisher'
require 'bloggit/plugin'
require 'bloggit/commandline'
require 'bloggit/server'
