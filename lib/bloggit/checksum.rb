require 'find'

module Bloggit
  module Checksum
    class << self

      # returns [files_to_upload, files_to_delete]
      def diff(source={}, target={})
        # look for differences...
        src_files = source.fetch('files', {})
        tgt_files = target.fetch('files', {})
        to_update = []; to_delete = []; to_upload = []

        tgt_files.each do |filename, checksum|
          if src_files.has_key? filename
            to_update << filename unless src_files[filename] == checksum
          else
            to_delete << filename
          end
        end

        to_upload = src_files.keys - tgt_files.keys

        # returns [files_to_upload, files_to_delete]
        [[to_upload, to_update].flatten, to_delete]
      end

      # Create the checksums...
      def generate_from(cache_dir)
        checksums = { 'generated_on'=>Time.now, 'files'=>{} }
        Find.find( cache_dir ) do |f|
          next if File.directory?( f )
          checksums['files'][f.gsub("#{cache_dir}/", '')] = Digest::MD5.hexdigest( File.read(f) )
        end
        checksums
      end
    end
  end
end