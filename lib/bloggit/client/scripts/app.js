Bloggit = (function(){
	
	var layout;
	// Panels/Tabs...
	var siteHeader, statusBar, homePage, tabPage, tabPosts,
		 tabThemes, tabMedia, tabPlugins, tabSettings, tabPreview;
	// Toolbars...
	var tbHomePage, tbPage, tbPosts, tbThemes, tbMedia, tbPlugins, 
		 tbSettings, tbPreview;
	// Grids
	var gridPosts, gridPages;
	// Tree
	var treePluging, treeMedia;
	// DataStores...
	var dsPosts = new Ext.data.Store({
		proxy: new Ext.data.HttpProxy({
			url: "/json/posts"
		}),
		reader: new Ext.data.JsonReader({
				totalProperty: 'length',
				root: 'results'
			}, 
			new Ext.data.Record.create([
				{name: 'title'},
				{name: 'slug'},
				{name: 'source'},
				{name: 'status'},
				{name: 'format'},
				{name: 'tags'},
				{name: 'publish_date'}
			])
		)
   });

	return {
		init: function() {

			layout = new Ext.BorderLayout(document.body, {
				north: {
					split: false,
					initialSize: 32,
					titlebar: false
				},
				south: {
					split: false,
					initialSize: 25,
					titlebar: false
				},
				center: {
					titlebar: false,
					autoScroll: true,
					closeOnTab: true,
					tabPosition: 'top'
				}
		  });

			tbPosts = new Ext.Toolbar('posts-tb', [
				{text:'New Post'},
				{text:'Delete', disabled:true},
				{text:'Send Pings', disabled:true}
			]);

			tbPages = new Ext.Toolbar('pages-tb', [
				{text:'New Page'},
				{text:'Delete', disabled:true}
			]);

			tbPreview = new Ext.Toolbar('preview-tb', [
				{text:'Refresh', disabled:true},
				{text:'Regenerate', disabled:true}
			]);


		
			var CP = Ext.ContentPanel;
			
			layout.beginUpdate();
			
			layout.add('north',  siteHeader = new CP('site-header', 'Bloggit') );
			layout.add('south',  statusBar = new CP('status-bar', 'Status') );
			layout.add('center', homePage = new CP('home-page', 'Home Page') );
			layout.add('center', tabPosts = new CP('tab-posts', 'Posts') );
			layout.add('center', tabPages = new CP('tab-pages', 'Pages') );
			layout.add('center', tabThemes = new CP('tab-themes', 'Themes') );
			layout.add('center', tabMedia = new CP('tab-media', 'Media') );
			layout.add('center', tabPlugins = new CP('tab-plugins', 'Plugins') );
			layout.add('center', tabSettings = new CP('tab-settings', 'Settings') );
			layout.add('center', tabPreview = new CP('tab-preview', 'Preview') );
			
			layout.getRegion('center').showPanel('home-page');

			layout.endUpdate();
			
			
			function titleRenderer(title) {
				return '<span style="font-size:115%; font-weight: bold;">'+ title +'</span>';
			}
			
			var cm = new Ext.grid.ColumnModel([
				{header: "Title", width: 300, dataIndex: 'title', id:'title', renderer:titleRenderer},
				{header: "Status", width: 115, dataIndex: 'status'},
				{header: "Format", width: 100, dataIndex: 'format'},
				{header: "Publish Date", width: 100, dataIndex: 'publish_date'},
			]);
	    cm.defaultSortable = true;

			dsPosts.load();
			// create the grid
			gridPosts = new Ext.grid.Grid('posts-grid', {
				ds: dsPosts,
				cm: cm,
				autoExpandColumn: 'title'
			});
			gridPosts.render();
			
			window.postsDS = dsPosts;
		}
	}
})();

Ext.EventManager.onDocumentReady(Bloggit.init, Bloggit, true);