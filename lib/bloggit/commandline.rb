require 'fileutils'
require 'cmdparse'

module Bloggit
  module Cmdline
    class << self
      
      def commands
        @commands ||= []
      end
      
      attr_accessor :verbose

      def register(&block)
        commands << block
      end
      
      def build(cmd, site=nil)
        commands.each do |block|
          block.call(cmd, site)
        end
      end
      
      def execute
        cmd = CmdParse::CommandParser.new( true, true )
        Bloggit::Cmdline.verbose = false

        cmd.program_name = "bloggit"
        cmd.program_version = Bloggit::RELEASE_INFO

        cmd.options = CmdParse::OptionParserWrapper.new do |opt|
          opt.separator "Global options:"
          opt.on("--verbose", "Be verbose when outputting info") {|t| Bloggit::Cmdline.verbose = true }
        end

        puts "Loading commands from #{File.join( File.dirname(__FILE__), 'commands', '*.rb')} (#{Bloggit::Cmdline.verbose})"
        Dir[ File.join( File.dirname(__FILE__), 'commands', '*.rb') ].each do |command|
          require command
        end

        site = Dir.getwd.ends_with?('.blog') ? Bloggit::Site.from_file(Dir.getwd) : nil
        
        Cmdline.build( cmd, site )

        cmd.add_command( CmdParse::HelpCommand.new )
        cmd.add_command( CmdParse::VersionCommand.new )

        cmd.parse
      end
    end
  end
end