Bloggit::Cmdline.register do |cmd, site|
  unless site.nil?

    page_cmd = CmdParse::Command.new( 'page', true, true )
    page_cmd.short_desc = "Page Management"
    cmd.add_command( page_cmd )

    add_page = CmdParse::Command.new( 'new', false )
    add_page.short_desc = "Create a new page"
    add_page.set_execution_block do |args|
      page = Bloggit::Page.to_file(args.to_s)
      puts "#{page.filename} created."
    end

    page_cmd.add_command( add_page )
  end
end
