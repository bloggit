Bloggit::Cmdline.register do |cmd, site|
  unless site.nil?

    post_cmd = CmdParse::Command.new( 'post', true, true )
    post_cmd.short_desc = "Post Management"
    cmd.add_command( post_cmd )

    add_post = CmdParse::Command.new( 'new', false )
    add_post.short_desc = "Create a new blog post"
    add_post.set_execution_block do |args|
      post = Bloggit::Post.to_file(args.to_s)
      puts "#{post.filename} created."
    end

    post_cmd.add_command( add_post )
  end
end
