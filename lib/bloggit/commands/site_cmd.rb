Bloggit::Cmdline.register do |cmd, site|
  site_cmd = CmdParse::Command.new( 'site', true, true )

  site_cmd.short_desc = "Site Management"

  cmd.add_command( site_cmd )

  if site.nil?

    new_site = CmdParse::Command.new( 'new', false )
    new_site.short_desc = "Create a site bundle"
    new_site.set_execution_block do |args|
    
      raise "Site name required!" if args.to_s.empty?
    
      name = args.to_s
      bundle_name = "#{name}.blog"
      current_dir = Dir.getwd
      bundle_path = File.join(current_dir, bundle_name)
        
      boiler_path = File.expand_path File.join( File.dirname(__FILE__), '..', 'boilerplate')
      
      FileUtils.mkdir_p( bundle_path, :verbose=>false)
      FileUtils.cp_r( Dir.glob(File.join(boiler_path, '*')), bundle_path, :verbose=>false)

      # After the boilerplate copy... Make sure all the folders are there
      dirs = [
        File.join(current_dir, bundle_name, 'cache'),
        File.join(current_dir, bundle_name, 'media'),
        File.join(current_dir, bundle_name, 'pages'),
        File.join(current_dir, bundle_name, 'posts'),
        File.join(current_dir, bundle_name, 'plugins'),
        File.join(current_dir, bundle_name, 'themes', 'default', 'images'),
        File.join(current_dir, bundle_name, 'themes', 'default', 'styles'),
        File.join(current_dir, bundle_name, 'themes', 'default', 'scripts'),
        File.join(current_dir, bundle_name, 'themes', 'default', 'templates', 'layouts'),
        File.join(current_dir, bundle_name, 'themes', 'default', 'templates', 'snippets')
      ]
          
      dirs.each do |path|
        FileUtils.mkdir_p( path, :verbose=>false)
      end

      puts "#{bundle_name} created."
    end

    site_cmd.add_command( new_site )
    
  else
    
    gen_site = CmdParse::Command.new( 'generate', false )
    gen_site.short_desc = "Generates site HTML in cache/ directory"
    gen_site.set_execution_block do |args|
      pub = Bloggit::Generator.new(site, Bloggit::Cmdline.verbose)
      pub.generate_cache
    end

    site_cmd.add_command( gen_site )
    
    # publish too?
    pub_site = CmdParse::Command.new( 'publish', false )
    pub_site.short_desc = "Uploads the HTML from cache/ to target server"
    pub_site.set_execution_block do |args|
      pub = Bloggit::Publisher.new(site, Bloggit::Cmdline.verbose)
      pub.upload
    end

    site_cmd.add_command( pub_site )
    
    # cleanup
    
    cleanup_cache = CmdParse::Command.new( 'cleanup', false )
    cleanup_cache.short_desc = "Removes files from cache/ directory"
    cleanup_cache.set_execution_block do |args|
      pub = Bloggit::Generator.new(site, Bloggit::Cmdline.verbose)
      pub.cleanup_cache
    end

    site_cmd.add_command( cleanup_cache )
    
  end
end