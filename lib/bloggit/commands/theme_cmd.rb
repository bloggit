Bloggit::Cmdline.register do |cmd, site|
  unless site.nil?

    theme_cmd = CmdParse::Command.new( 'theme', true, true )
    theme_cmd.short_desc = "Theme Management"
    cmd.add_command( theme_cmd )

    new_theme = CmdParse::Command.new( 'new', false )
    new_theme.short_desc = "Create a new theme"
    new_theme.set_execution_block do |args|
      #page = Bloggit::Page.to_file(args.to_s)
    end

    theme_cmd.add_command( new_theme )
  end
end
