class Hash

  # Extends a hash so that it can be used like an object in a template context (e.g. +hash.key+)
  def method_missing(name, *args)
    if name.to_s.ends_with? '='
      name = name.to_s[0..-2]
      self[name] = args[0]
    else
      if self.has_key? name.to_s
        self[name.to_s]
      elsif self.has_key? name.to_sym
        self[name.to_sym]
      else
        super(name,*args)
      end
    end
  end

  # Returns the binding so you can retrieve values from it...
  def get_binding
    binding
  end

end
