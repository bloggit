module Bloggit
  class << self
    def listeners
      @listeners ||= {}
    end
    
    def on_event(event, &block)
      listeners[event] ||= []
      listeners[event] << block
    end
    
    def fire_event(event, *args)
      results = {}
      listeners.fetch(event, []).each do |block|
        res = block.call(*args)
        results.merge!(res) if res.is_a?( Hash )
      end
      results
    end    
  end
end