# See Bloggit::Media
module Bloggit
  
  # = Media
  class Media
    
    attr_reader :site, :filename, :ext, :full_path, :relative_path, :name, :size, :dimensions
    
    def initialize(path, site)
      @site = site
      @full_path = path
      @relative_path = path.gsub(File.join(site.base_path, 'media', ''), '')
      # Read file info and set Media properties...
      @filename = File.basename(path)
      @ext = File.extname(path)
      # TODO: name, size, dimensions
    end
    
    def thumbnail_path(size=nil)
    end

    def create_thumbnail(size=nil)
    end
    
    def permalink
      "media/#{@relative_path}"
    end
    
    class << self
      #private :new
      
      # Creates a Media object from an image on the filesystem
      def from_file(path, site) 
        path = File.expand_path(path)
        raise "File must exist" unless File.exists?( path )
        new( path, site )
      end
    end
  end
end