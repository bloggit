# See Bloggit::Page
module Bloggit
  
  # = Page
  #
  # A Page is much like a Post (Bloggit::Post)
  class Page
    attr_reader :filename, :source, :site
    
    def initialize(site, filename, body, atts={})
      @site = site unless site.nil?
      @filename = filename
      @data = atts.clone
      @data.reverse_merge!(site.settings.pages) unless site.nil?
      
      @source = body

      @data.slug = @filename.split('_')[-1].gsub('.page', '') unless @data.has_key? 'slug'
      @data.guid = @filename.split('_')[0] unless @data.has_key? 'guid'
      @data.title = "#{@data.slug.gsub('-', ' ')}".titleize unless @data.has_key? 'title'
      @data.status = 'publish' unless @data.has_key? 'status'

      if @data.has_key? 'tags'
        @data.tags.uniq!
        @data.tags.each { |tag| Tag.register_page( tag, self ) if self.is_publishable? }
      else
        @data.tags = []
      end
    end
    
    def is_draft?
      @data.status == 'draft'
    end
    
    def is_publishable?
      !is_draft?
    end
    
    def has_tags?
      @data.has_key? 'tags'
    end

    def permalink
      "#{@data.slug}.html"
    end
    
    def render_content(template_binding=nil)
      src = @source
      src = Template.from_text(src).template.result(template_binding) unless template_binding.nil?
      format = @data.has_key?('format') ? @data['format'].to_s.downcase.to_sym : :simple
      TextFormatter.render(src, format)
    end
        
    def to_yaml    
      yml_s = @data.to_yaml
      yml_s << @source.to_yaml
    end
    
    def to_json
    	data_hsh = @data.clone
      data_hsh['source'] = @source
      data_hsh.to_json
    end
    
    def method_missing(name,*args)
      if @data.has_key? name.to_s
        @data[name.to_s]
      else
        super(name, *args)
      end
    end
    
    def [](name)
      if @data.has_key? name.to_s
        @data[name.to_s]
      else
        nil
      end
    end
    
    class << self
      private :new
      
      def find_by_tag(tag, site=nil)
        find_by_tags( [tag] )
      end
      
      def find_by_tags(tags=[], site=nil)
        site = Site.active if site.nil?
        pages = site.pages.clone
        tags = [tags].flatten

        pages.delete_if do |p| 
          unless p.tags.nil?
            tag_diff = [p.tags].flatten & tags
            tag_diff.length != tags.length
          else
            true
          end
        end
        
        pages
      end
      
      def from_file(path, site=nil)
        path = File.expand_path(path)
        raise "File must exist" unless File.exists?( path )
        yml_docs = YAML::load_stream( File.open(path, 'r') )
        raise "page is missing a document part" if yml_docs.documents.length != 2
        raise "page format is invalid" unless yml_docs[0].is_a?(Hash) and yml_docs[1].is_a?(String)
        atts = yml_docs[0]
        body = yml_docs[1]
        new( site, File.basename(path), body, atts )
      end
      
      def to_file(title, site=nil, options={})
        slug = title.to_s.to_slug
        
        path = "#{Site.pages.length}_#{slug}.page"
        page = new( site, path, "New Page\n\n", 'slug'=>slug, 'title'=>title, 'guid'=>Site.pages.length )
        new_page_path = File.join(Dir.getwd, 'pages', path)
        raise "Path Exception! #{new_page_path} already exists." if File.exists?(new_page_path)
        File.open( new_page_path, 'w' ) {|f| f.write page.to_yaml }
        page
      end
    end
  end
  
end