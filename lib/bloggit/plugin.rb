# See Bloggit::Plugin
require 'test/unit/assertions'

module Bloggit
  
  # = Plugin
  #
  #   Plugin.define :my_plugin do |conf|
  #     # default plugin settings...
  #   end
  class Plugin
    include Test::Unit::Assertions
    
    class << self
      attr_reader :site, :plugins
      
      def init(site) #:nodoc:
        @site = site
        @plugins = {}
        # Load/require plugins from plugins/ dir...
        @plugin_path = File.join(site.base_path, 'plugins')
        $: << @plugin_path
        Dir[ File.join(@plugin_path, '*') ].each do |dir|
          plugin_file = if FileTest.directory?(dir)
            plugin_lib_dir = File.join(dir, 'lib')
            $: << plugin_lib_dir if FileTest.directory?(plugin_lib_dir)
            "#{File.basename(dir)}/init"
          elsif dir.ends_with?('.rb') or dir.ends_with?('.plugin')
            "#{File.basename(dir)}"
          end
          require(plugin_file)
        end
      end
      
      def register(plugin_name, opts={}, &block)
        # Yeah...
        @plugins[plugin_name] = block.call( settings_for(plugin_name) )
      end
      
      def settings_for(plugin_name)
        @site.settings.plugins.fetch( plugin_name.to_s, {} )
      end
      
      def active_site
        @site
      end
      
      def [](key)
        @plugins[key]
      end
      
    end
  end
  
end