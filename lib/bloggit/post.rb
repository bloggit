# See Bloggit::Post
module Bloggit
  
  # = Post
  # 
  # A blog post is a YAML file containing two documents. The first document should be 
  # a hash of attributes, the second a string document. The format looks a little like 
  # the following:
  #
  #   title: To Boldly Go
  #   publish_date: 3/07/2004 11:30:00 -0500
  #   format: Markdown
  #   tags: [one, two, three and four]
  #   slug: to-boldly-go
  #   --- |
  #   This is the post body... Fun, isn't it? Yes. Yes it is.
  #   
  #   And you know, I don't care if anybody knows! :D
  #
  # The filename is the date and slugline (YYYY.MM.DD_slug.post)
  class Post
    attr_accessor :filename, :source
    
    def initialize(site, filename, body, atts={})
      @site = site
      @filename = filename
      @data = atts.clone
      @data.reverse_merge!(site.settings.posts) unless site.nil?
      @source = body.to_s
      
      # date_str = if atts.has_key? 'publish_date'
      #         atts.publish_date #.to_s.gsub('.','-')
      #       else
      #         @filename.split('_').to_s.gsub('.','-')
      #       end
      
      begin
        publish_date = atts.publish_date.is_a?( Time ) ? atts.publish_date : Time.parse( atts.publish_date )
      rescue
        raise "#{@filename}: Date format error!"
      end

      @data.publish_date = publish_date
      @data.post_year    = publish_date.year
      @data.post_month   = publish_date.month
      @data.post_date    = publish_date.day
      @data.slug = @filename.split('_')[-1].gsub('.post', '') unless @data.has_key? 'slug'
      @data.guid = @filename.split('_')[0] unless @data.has_key? 'guid'
      @data.status = 'publish' unless @data.has_key? 'status'
      @data.tags = [] unless @data.has_key? 'tags'
      @data.tags.uniq!
      @data.tags.each {|tag| Tag.register_post( tag, self )  } if self.is_publishable?
    end
    
    def is_draft?
      @data.status == 'draft'
    end
    
    def is_publishable?
      !is_draft? and @data.publish_date <= Time.now
    end
    
    def has_tags?
      @data.has_key? 'tags'
    end
    
    def tags
      @data.tags
    end
    
    def permalink
      @site.build_post_path("#{@data.post_year}/#{@data.post_month}/#{@data.slug}.html")
    end
    
    # Navigation
    def has_next?
      idx = @site.posts.index(self) +1
      idx < @site.posts.length
    end
    
    def has_previous?
      idx = @site.posts.index(self)
      idx > 0
    end
    
    def first
      @site.posts[0]
    end
    
    def previous
      idx = @site.posts.index(self) -1
      @site.posts[idx]
    end
    
    def next
      idx = @site.posts.index(self) +1
      @site.posts[idx]
    end
    
    def last
      @site.posts[@site.posts.length() -1]
    end
    
    def render_content(template_binding=nil)
      src = @source
      src = Template.from_text(src).template.result(template_binding) unless template_binding.nil?
      TextFormatter.render(src, @data.fetch('format', 'simple'))
    end
    
    def to_yaml
      yml_s = prepare_data.to_yaml
      yml_s << @source.to_yaml
    end
    
    def to_json
    	data_hash = prepare_data
      data_hash['source'] = @source
      data_hash.to_json
    end
    
    def method_missing(name,*args)
      if @data.has_key? name.to_s
        @data[name.to_s]
      else
        super(*args)
      end
    end
    
    def [](name)
      if @data.has_key? name.to_s
        @data[name.to_s]
      else
        nil
      end
    end
    
    
  private
    
    def prepare_data
      data_hsh = @data.clone
      data_hsh.delete 'post_year'
      data_hsh.delete 'post_month'
      data_hsh.delete 'post_date'
      data_hsh.publish_date = data_hsh.publish_date.strftime('%m/%d/%Y %H:%M:%S %z')
      data_hsh
    end
    
    
    class << self
      private :new
      
      def find_by_tag(tag, site=nil)
        find_by_tags( [tag] )
      end
      
      def find_by_tags(tags=[], site=nil)
        site = Site.active if site.nil?
        posts = site.posts.clone
        tags = [tags].flatten

        posts.delete_if do |p| 
          unless p.tags.nil?
            tag_diff = [p.tags].flatten & tags
            tag_diff.length != tags.length
          else
            true
          end
        end
        
        posts
      end
      
      def from_file(path, site=nil)
        path = File.expand_path(path)
        raise "File must exist" unless File.exists?( path )
        yml_docs = YAML::load_stream( File.open(path, 'r') )
        raise "Post is missing a document part" if yml_docs.documents.length != 2
        raise "Post format is invalid" unless yml_docs[0].is_a?(Hash) and yml_docs[1].is_a?(String)
        atts = yml_docs[0]
        body = yml_docs[1]
        
        new( site, File.basename(path), body, atts )
      end
      
      def to_file(title, site=nil, options={})
        slug = title.to_s.downcase.gsub( /[^-a-z0-9~\s\.:;+=_]/, '').gsub(/[\s\.:;=_+]+/, '-').gsub(/[\-]{2,}/, '-').to_s
        raise "Slug can't be empty" if slug.empty?
        path = "#{Site.posts.length}_#{slug}.post"
        post = new( site, path, "New Post\n\n", 'slug'=>slug, 'title'=>title, 'publish_date'=>Time.now, 'guid'=>Site.posts.length )
        new_post_path = File.join(Dir.getwd, 'posts', path)
        raise "Path Exception! #{new_post_path} already exists." if File.exists?(new_post_path)
        File.open( new_post_path, 'w' ) {|f| f.write post.to_yaml }
        post
      end
    end
  end
end