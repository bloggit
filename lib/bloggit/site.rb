# See Bloggit::Site
module Bloggit
  
  # = Site
  # 
  # A site is stored as a collection of folders and files with the following
  # layout:
  # 
  #   Site.blog/      - Bundle
  #     posts/        - All the Posts stored as text files prefixed with data info YYMMDD-slug
  #     pages/        - Static Pages
  #     media/        - All images, movies, sound files, etc.,
  #     templates/    - Templates for rendering the site
  #     cache/        - The latest rendered output
  #     settings.yml  - Site-wide settings
  #     Rakefile      - Defines tasks for generating cache, and publishing it
  # 
  class Site
    attr_accessor :settings, :pages, :posts, :media, :templates
    attr_reader :base_path, :all_pages, :all_posts
    
    def initialize(base_path, ignore_media=false)
      @base_path = base_path
      
      # Read settings.yml from folder...
      @settings = YAML::load( File.open(File.join(base_path, 'settings.yml'), 'r') )
      
      Tag.reset!
      
      # Load Pages...
      @pages = []
      @page_hsh = {}
      @inactive_pages = []
      Dir.glob( File.join(base_path, 'pages', '**', '*.page') ).each do |page_path|
        page = Page.from_file(page_path, self)
        if page.is_publishable?
          @pages << page
          @page_hsh[page.slug] = page
        else
          @inactive_pages << page
        end
      end
      @all_pages = @pages + @inactive_pages

      @pages.sort! {|a,b| a.title <=> b.title }
      @all_pages.sort! {|a,b| a.title <=> b.title }
      
      # Load Posts...
      @posts = []
      @inactive_posts = []
      @post_hsh = {}
      Dir.glob( File.join(base_path, 'posts', '**', '*.post') ).each do |post_path|
        post = Post.from_file(post_path, self)
        if post.is_publishable?
          @posts << post
          @post_hsh[post.slug] = post
        else
          @inactive_posts << post
        end
      end
      @all_posts = @posts + @inactive_posts

      @posts.sort! {|a,b| a.publish_date <=> b.publish_date }.reverse!
      @all_posts.sort! {|a,b| a.publish_date <=> b.publish_date }.reverse!
      
      # Load Media... (?)
      @media = {}
      Dir.glob( File.join(base_path, 'media', '**', '*') ).each do |media_path|
        unless FileTest.directory?(media_path)
          media_item = Media.from_file(media_path, self)
          @media[ media_item.relative_path ] = media_item
        end
      end unless ignore_media
      
      # Load Templates... (?)
      #@templates = []
      
      # Load plugins
      Plugin.init(self)
    end
    
    def blog_path 
      @settings.site.fetch('blog_root', '')
    end
    
    def home_page
      @settings.site.fetch('homepage', 'blog')
    end
    
    def tags
      Tag.tag_list.values.sort {|a,b| a.name <=> b.name }
    end
    
    def has_tags?
      !Tag.tag_list.nil?
    end
    
    def get_page_by_slug(slug)
      @page_hsh[slug]
    end

    def get_post_by_slug(slug)
      @post_hsh[slug]
    end
    
    def latest_posts(limit=nil)
      limit = @settings.syndication.fetch('number_of_entries', 10) if limit.nil?
      @posts[0..limit]
    end

    def latest_posts_excluding_tag(limit=nil, tag=nil)
      limit = @settings.syndication.fetch('number_of_entries', 10) if limit.nil?
      latest = @posts.select do |post|
        if post.has_tags? && !post.tags.include?(tag)
          true
        else
          false
        end
      end
      latest[0..limit]
    end

    def latest_posts_with_tag(limit=nil, tag=nil)
      limit = @settings.syndication.fetch('number_of_entries', 10) if limit.nil?
      latest = @posts.select do |post|
        if post.has_tags? && post.tags.include?(tag)
          true
        else
          false
        end
      end
      latest[0..limit]
    end
    
    def absolute_path_to(obj)
      case obj.class.to_s
        when 'Bloggit::Post'
          build_path(obj.permalink)
          
        when 'Bloggit::Page'
          build_path(obj.permalink)
          
        when 'Bloggit::Tag'
          build_path(obj.permalink)

        when 'Bloggit::Media'
          build_path(obj.permalink)
          
        when 'Symbol'
          case obj
            when :feed: build_path( @settings.syndication.filename )
            when :home: build_path( 'index.html' )
            when :blog: build_path( build_post_path('index.html') )
          end
        else
          puts "Can't build a path for #{obj}/#{obj.class}"
          '?type=unknown'
      end
    end
    
    def plugin_settings(plugin_name)
      @settings.plugins.fetch(plugin_name.to_s, {})
    end
    
    def method_missing(name, *args)
      if @settings.site.has_key?(name.to_s)
        @settings.site[name.to_s]
      else
        puts "Site: #{name}?"
        super(name, *args)
      end
    end
    
    def build_post_path(path)
      base_path = ''
      if blog_path != ''
        base_path = blog_path
        base_path += '/' unless base_path.ends_with?('/')
      end
      base_path + path
    end
    
  private
    
    def build_path(*segs)
      base_url = @settings.syndication.fetch('base_url', '')
      segs.unshift(base_url)
      segs.join('/')
    end
    
    class << self
      protected :new
      attr_accessor :active
      
      def from_file(path, ignore_media=false)
        path = File.expand_path(path)
        raise "Path must be a directory" unless FileTest.directory? path
        returning Site.new(path, ignore_media) do |site|
          Site.active = site
        end
      end
      
      def create(path)
        # Create folder structure and initial files...
        path = File.expand_path(path)
        raise "Path must be a directory" unless FileTest.directory? path
        #FIXME: Create folders and such... Use generator?
      end
    end
  end
end