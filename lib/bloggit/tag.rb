# See Bloggit::Tag
module Bloggit
  # = Tag
  class Tag
    attr_accessor :name, :pages, :posts
    
    def initialize(tag_name)
      @name = tag_name
      @pages = []
      @posts = []
    end
    
    def has_pages?
      @pages.length > 0
    end
    
    def has_posts?
      @posts.length > 0
    end
    
    def slug
      @name.to_slug
    end
    
    def permalink
      "tags/#{slug}.html"
    end
    
    def add_page(page)
      @pages << page
      @pages.sort! {|a,b| a.title <=> b.title }.reverse!
    end
    
    def add_post(post)
      @posts << post
      @posts.sort! {|a,b| a.publish_date <=> b.publish_date }.reverse!
    end
    
    class << self
      private :new
      attr_reader :tag_list
      
      def reset!
        @tag_list = {}
      end
      
      def register_page(tag, page)
        @tag_list ||= {}
        @tag_list[tag] = new(tag) unless @tag_list.has_key? tag
        @tag_list[tag].add_page page
        
      end
      
      def register_post (tag, post)
        @tag_list ||= {}
        @tag_list[tag] = new(tag) unless @tag_list.has_key? tag
        @tag_list[tag].add_post post
      end
    end
  end
end