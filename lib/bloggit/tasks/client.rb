require 'rake'
require 'bloggit'

desc "Runs the (Browser) client"
task :client do
	# This is ugly -- find a better way!
	fork do
    Bloggit::Server.run()
  end
  
  # A pathetic hack to delay for a fraction of second the starting of the browser...
  1000000.times {|i| i.to_s }
  
  begin
	  `open http://127.0.0.1:3322`
  rescue
    # This may fail ???
  end

	Process.wait
end