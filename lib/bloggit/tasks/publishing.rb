# Some rake tasks for generating the site HTML and publishing to a local and/or remote location
require 'rake'
require 'bloggit'

VERBOSE = true unless defined?(VERBOSE)

task :default=>:generate

desc "Generate static HTML in cache"
task :generate=>[:cleanup] do
  # Generate the HTML site and structure under cache/
  site = Bloggit::Site.from_file('.') #File.dirname( __FILE__ )
  gen = Bloggit::Generator.new(site, VERBOSE)
  gen.generate_cache
end

desc "Upload cache to target location"
task :upload do
  # Copy/SFTP/FTP site from cache/ to target location defined in settings.yml (?)
  site = Bloggit::Site.from_file('.') #File.dirname( __FILE__ )
  pub = Bloggit::Publisher.new(site, VERBOSE)
  pub.upload
end

desc "Generates website and uploads it to the target location"
task :publish=>[:generate, :upload] do
  puts "Finished"
end

desc "Removes site cache"
task :cleanup do
  # Kill the files/folders under cache/
  site = Bloggit::Site.from_file('.', true) #File.dirname( __FILE__ )
  pub = Bloggit::Generator.new(site, VERBOSE)
  pub.cleanup_cache
end

task :clean=>:cleanup do; end
