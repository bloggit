# See Bloggit::TextFormatter

require 'redcloth'
require 'bluecloth'

module Bloggit
  # = TextFormatter
  #
  # Default text formatters are: :textile, :markdown, and :simple
  #
  module TextFormatter
    class << self
      
      # Registers a formatter block with a given name.
      #
      # In pages or posts, you can specify the text format in the header using:
      #
      #   format: Textile+.
      #
      # Bloggit will look for a TextFormatter that's been registered as :textile
      def register(name, &block)
        name = name.to_s.downcase.to_sym
        @formatters ||= {}
        @formatters[name] = block
      end
      
      # Returns the processed text using the specified format, or the original text
      # if the format isn't recognized.
      def render(text, format)
        format = format.to_s.downcase.to_sym #unless format.is_a?(Symbol)
        formatter = @formatters.fetch(format, Proc.new {|text| raise "Could not format text as '#{format}'"  })
        formatter.call( text )
      end
      
    end
    
  end
end

Bloggit::TextFormatter.register :textile do |text|
  RedCloth.new(text).to_html
end

Bloggit::TextFormatter.register :markdown do |text|
  BlueCloth.new(text).to_html
end

Bloggit::TextFormatter.register :simple do |text|
  text.gsub("\n", "<br/>\n")
end

Bloggit::TextFormatter.register :none do |text|
  text
end