# = Examples for creating your own plugins...

include Bloggit

# = Registering the plugin to retrieve settings from settings.yml
Plugin.register :example do |settings|
  # this will read from the settings.yml under plugins.example
  #  $username = settings.fetch('username', '')
end


# = Registering custom tags in the template engine
#
# This will register the tag `example` for use in any template page 
# (including layouts). Example usage of this tag:
#
#   <%= example :version %>
#
# Output:
#
#  Bloggit 1.0.0
#
Template.register_tag :example do |*args|
  type, opts = args
  opts ||= {}

  case type
    when :version
      "Bloggit #{Bloggit::RELEASE_INFO}"
    else
      "Unknown tag #{ type }"
  end
    
end

# = Responding to application events...
Bloggit::on_event(:after_generate) do |site, cache_dir|
  # puts "I'm called after the rest of the site has been generated..."
end

# = Adding a command line tool...
Cmdline.register do |cmd, site|
   example_cmd = CmdParse::Command.new( 'example', true, true )
   example_cmd.short_desc = "Example tool from plugin"
   cmd.add_command( example_cmd )

   sub_cmd = CmdParse::Command.new( 'sub', false )
   sub_cmd.short_desc = "A sub command"
   sub_cmd.set_execution_block do |args|
     puts "Sub-command has been called, w00t!"
   end

   example_cmd.add_command( sub_cmd )
end

# = Registering a custom text format:
begin
  require 'rdoc/markup/simple_markup'
  require 'rdoc/markup/simple_markup/to_html'

  Bloggit::TextFormatter.register :rdoc do |text|
    p = SM::SimpleMarkup.new
    h = SM::ToHtml.new
    p.convert(text, h)
  end

rescue LoadError
  puts "There was an error registering the :rdoc text format (#{$!})"
end