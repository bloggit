# This is required automatically..

Bloggit::Plugin.register :haloscan do |settings|
  $username = settings.fetch('username', '')
end

Bloggit::Template.register_tag :haloscan do |*args|
  type, slug = args
  case type

    when :comments
      %Q|<a href="javascript:HaloScan('#{slug}');" target="_self"><script type="text/javascript">postCount('#{slug}');</script></a>|
      
    when :trackbacks
      %Q|<a href="javascript:HaloScanTB('#{slug}');" target="_self"><script type="text/javascript">postCountTB('#{slug}'); </script></a>|
      
    when :script
      %Q|<script type="text/javascript" src="http://www.haloscan.com/load/#{$username}"> </script>|

    else
      'WHAT ELSE?'
  end 
end