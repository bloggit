# This is required automatically..

Bloggit::Plugin.register :js_search do |settings|

end

Bloggit::Template.register_tag :js_search do |*args|
  type, label = args
  label ||= 'Search'
  path = Bloggit::Template.current_template
  case type

    when :form
      %Q|<form id="search-form" action="#" method="GET" onsubmit="Search.query(document.getElementById('q').value);return false;"><input class="search-query" type="search" name="q" id="q" placeholder=""/><input class="search-button" type="submit" value="#{label}"/></form>|

    when :script
      %Q|<script src="#{path.url_for :script=>'search-engine'}"></script><script>Search.index_uri='#{path.url_for :script=>'keyword-index'}';</script>|
    else
      '?'
  end 
end

NOISE_WORDS = %w(about after all also an and another any are as at be because been before
being between both but by came can come could did do each for from get
got has had he have her here him himself his how if in into is it like
make many me might more most much must my never now of on only or other
our out over said same see should since some still such take than that
the their them then there these they this those through to too under up
very was way we well were what where which while who with would you your a
b c d e f g h i j k l m n o p q r s t u v w x y z $ 1 2 3 4 5 6 7 8 9 0 _)

Bloggit::on_event(:after_generate) do |site, cache_dir|
  puts "Generating search index..."
  
  index_data = {
    :count=>0,
    :pages=>[]
  }
  
  site.pages.each do |page|
    msg = page.source.downcase
    total_words = msg.split(' ').length
    words = msg.gsub!( /[\W|_]/, ' ' ).split(' ')
    words.uniq!
    words.delete_if {|word| NOISE_WORDS.include?( word ) }
    # TODO: Add path key that's the path from ROOT...
    index_data[:pages] << { :title=>page.title,  'url'=>site.absolute_path_to(page), 'keywords'=>words.join(' '), :type=>'page' }
    puts "  #{words.length.to_s.rjust(3,' ')}/#{total_words.to_s.ljust(3,' ')} - #{page.title} (page)"
  end

  site.posts.each do |post|
    msg = post.source.downcase
    msg += " #{post.tags.join(' ')} #{post.title}"
    total_words = msg.split(' ').length
    words = msg.gsub!( /[\W|_]/, ' ' ).split(' ')
    words.uniq!
    words.delete_if {|word| NOISE_WORDS.include?( word ) }
    # TODO: Add path key that's the path from ROOT...
    index_data[:pages] << { :title=>post.title,  'url'=>site.absolute_path_to(post), 'keywords'=>words.join(' '), :type=>'post' }
    puts "  #{words.length.to_s.rjust(3,' ')}/#{total_words.to_s.ljust(3,' ')} - #{post.title} (post)"
  end
  
  index_data[:count] = index_data[:pages].length
  File.open( File.join(cache_dir, 'theme', 'scripts', 'keyword-index.js'), 'w') do |f|
    f.write index_data.to_json
  end
  
  FileUtils.cp File.join(site.base_path, 'plugins', 'js_search', 'scripts', 'search-engine.js'), File.join(cache_dir, 'theme', 'scripts', 'search-engine.js')
end