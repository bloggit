// Simple Keyword Search Engine
//
//   Usage:
//			Search.query(keyword) - returns array of matched pages

window.Search = {
	index_uri :'keyword-index.js', // Path to the keyword-index
	_idx :null,

	query :function(q) {
		try {
			this.fetch_index();

		  var re = new RegExp(q, 'gi');
			var results = [];

		  for(var i=0; i<this._idx.pages.length; i++) {
				var page = this._idx.pages[i];
		    if( re.test(page.keywords) ) {
					results.push( page );
		    }
		  }

			this._onSearchComplete(results, q);
		  return results;			
		} catch (err) {
			// If something went wrong... punt!
			this._onError(err);
		}
	},

	onFetchIndex :function(callback) {
		this._onFetchIndex = callback;
	},
	_onFetchIndex :function(complete){},

	onSearchComplete :function(callback) {
		this._onSearchComplete = callback;
	},
	_onSearchComplete: function(results, q){  },

	onError :function(callback) {
		this._onError = callback;
	},
	_onError: function(err){ alert(err.message || err.description || err); },

	fetch_index :function() {
		if(this._idx == null) {
			this._onFetchIndex(false);
		  var xhr = this.get_xhr();
	    xhr.open("GET", this.index_uri, false); // Synchronous loading...
	    xhr.send("");
		  eval("window.Search._idx = "+ xhr.responseText);
			this._onFetchIndex(true);
		}
	},
	get_xhr :function() {
		return this.one_of_these(
			function() {return new XMLHttpRequest()},
    	function() {return new ActiveXObject('Msxml2.XMLHTTP')},
    	function() {return new ActiveXObject('Microsoft.XMLHTTP')}
		);
	},
	one_of_these :function() {
    var returnValue;
    for (var i=0; i<arguments.length; i++) {
      try { returnValue = arguments[i](); break; } catch (ignore) {};
    }
    return returnValue;
	}
}