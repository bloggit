begin

  require 'rdoc/markup/simple_markup'
  require 'rdoc/markup/simple_markup/to_html'

  Bloggit::TextFormatter.register :rdoc do |text|
    p = SM::SimpleMarkup.new
    h = SM::ToHtml.new
    p.convert(text, h)
  end

rescue LoadError
  puts "There was an error registering the :rdoc text format (#{$!})"
end