require File.dirname(__FILE__) + '/../helpers'
require 'bloggit'

class PageTest < Test::Unit::TestCase
  
  should "raise an error if trying to load a `Page` from a non-existant file" do
    assert_raise(RuntimeError) { Bloggit::Page.from_file 'crapfile.page' }
  end
  
  should "parse `Page` from file" do
    assert_nothing_raised(RuntimeError) do
      page = Bloggit::Page.from_file File.join(File.dirname(__FILE__), '../fixtures/test.blog/pages/about.page')
      assert_not_nil page
      assert_equal 'about', page.slug
    end
  end
  
  should "parse status and slug" do
    assert_nothing_raised(RuntimeError) do
      page = Bloggit::Page.from_file File.join(File.dirname(__FILE__), '../fixtures/test.blog/pages/about.page')
      assert_not_nil page
      assert_equal 'About', page.title
      assert_equal 'about', page.slug
      assert_equal 'publish', page.status
    end
  end
  
  should "allowing finding pages by tag" do
    site = Bloggit::Site.from_file(File.join(File.dirname(__FILE__), '../fixtures/test.blog') )
    pages = Page.find_by_tag 'todo'

    assert_equal 1, pages.length
    assert_equal "To Do", pages[0].title
  end
  
end