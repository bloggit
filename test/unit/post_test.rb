require File.dirname(__FILE__) + '/../helpers'
require 'bloggit'

class PostTest < Test::Unit::TestCase
  
  should "raise an error if trying to load a `Post` from a non-existant file" do
    assert_raise(RuntimeError) { Bloggit::Post.from_file 'crapfile.post' }
  end
  
  should "parse `Post` from file" do
    assert_nothing_raised(RuntimeError) do
      post = Bloggit::Post.from_file File.join(File.dirname(__FILE__), '../fixtures/test.blog/posts/001_to-boldly-go.post')
      assert_not_nil post
      assert_equal 'to-boldly-go', post.slug
      assert_equal 2004, post.post_year
    end
  end
  
  should "parse publish date and slug" do
    assert_nothing_raised(RuntimeError) do

      post = Bloggit::Post.from_file File.join(File.dirname(__FILE__), '../fixtures/test.blog/posts/001_to-boldly-go.post')
      assert_not_nil post
      assert_equal 'to-boldly-go', post.slug
      assert_equal 2004, post.post_year
      assert_equal Time, post.publish_date.class

      post = Bloggit::Post.from_file File.join(File.dirname(__FILE__), '../fixtures/test.blog/posts/002_sure-whatever.post')
      assert_not_nil post
      assert_equal 'sure-whatever', post.slug
      assert_equal 2007, post.post_year
      assert_equal Time, post.publish_date.class
    end
  end
  
  should "allowing finding by tag" do
    site = Bloggit::Site.from_file(File.join(File.dirname(__FILE__), '../fixtures/test.blog') )
    posts = Post.find_by_tag 'two'
    assert_equal 2, posts.length

    posts = Post.find_by_tags ['two', 'one']
    assert_equal 2, posts.length

    posts = Post.find_by_tag ['one']
    assert_equal 3, posts.length

    posts = Post.find_by_tag ['rdoc']
    assert_equal 1, posts.length
  end
  
  
end