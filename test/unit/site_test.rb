require File.dirname(__FILE__) + '/../helpers'
require 'bloggit'

class SiteTest < Test::Unit::TestCase

  should "not allow `Site.new`" do
    assert_raise(NoMethodError) { Bloggit::Site.new }
  end
  
  should "not create a `Site` object unless path is a directory" do
    assert_raise(RuntimeError) { Bloggit::Site.from_file(__FILE__) }
  end

  should "create a `Site` object from a file path" do
    site = Bloggit::Site.from_file(File.join(File.dirname(__FILE__), '../fixtures/test.blog') )
    assert_not_nil site
    assert site.is_a?( Bloggit::Site )
    assert_equal 3, site.posts.length
    assert_equal Time, site.posts[0].publish_date.class
    assert_not_nil site.settings
    assert_equal 'My Site', site.settings['site']['title']
    # Test Hash extenstion too..
    assert_equal 'My Site', site.settings.site.title
    assert_equal 'My Site', site.title
  end
  
end