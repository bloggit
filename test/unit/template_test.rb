require File.dirname(__FILE__) + '/../helpers'
require 'bloggit'

class TemplateTest < Test::Unit::TestCase

  def setup
    site = Bloggit::Site.from_file(File.join(File.dirname(__FILE__), '../fixtures/test.blog') )
    Template.config do |t|
      t.site = site
    end
  end

  should "render templates from text" do
    output = Template.from_text('Hello, <%= name %>').render :template=>{:name=>'Sally'}
    assert "Hello, Sally", output
  end

  should "render templates from file" do
    output = Template.from_file( File.join( File.dirname(__FILE__), '..', 'fixtures', 'template.html.erb') ).render :template=>{:name=>'Bob'}
    assert "Hello, Bob", output
  end

  should "generate URLs correctly"

end