require File.dirname(__FILE__) + '/../helpers'
require 'bloggit/text_formatter'

class TextFormatterTest < Test::Unit::TestCase

  should "render text using Textile" do
    keys = [:textile, :Textile, :teXtile, 'Textile', 'teXTIle']
    keys.each do |key|
      output = Bloggit::TextFormatter.render('Hello, *Sally*', key)
      assert_equal "<p>Hello, <strong>Sally</strong></p>", output
    end
  end

  should "render text using Markdown" do
    keys = [:markdown, :Markdown, :markDOWN, 'Markdown', 'MarKDowN']
    keys.each do |key|
      output = Bloggit::TextFormatter.render('Hello, *Sally*', key)
      assert_equal "<p>Hello, <em>Sally</em></p>", output
    end
  end
  
  should "raise an error when trying to use an undefined format" do
    assert_raise(RuntimeError) { Bloggit::TextFormatter.render('Hello, *Sally*', :cpt_gloval) }
  end
  
  should "allow registration of custom text formatter" do
    # Register Formatter...
    Bloggit::TextFormatter.register :rdoc do |text|
      require 'rdoc/markup/simple_markup'
      require 'rdoc/markup/simple_markup/to_html'

      p = SM::SimpleMarkup.new
      h = SM::ToHtml.new
      p.convert(text, h)
    end

    assert_nothing_raised(RuntimeError) do
      output = Bloggit::TextFormatter.render('Hello, *Sally*', :rdoc)
      assert_equal "<p>\nHello, <b>Sally</b>\n</p>\n", output
    end
  end

end